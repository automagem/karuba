var _ = require('lodash')
var $ = require('jquery')

var Setup = {
  locations: [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110],
  temples: ['blue temple', 'yellow temple', 'purple temple', 'brown temple'],
  explorers: ['blue explorer', 'yellow explorer', 'purple explorer', 'brown explorer'],
  colors: ['blue', 'yellow', 'purple', 'brown'],
  newSetup: function () {
    var explorerLocation = _.shuffle(this.locations)
    var templeLocation = _.shuffle(this.locations)
    // get explorers and temple initial locations:
    var setup = {
      blue: { temple: templeLocation[0], explorer: explorerLocation[0] },
      yellow: { temple: templeLocation[1], explorer: explorerLocation[1] },
      purple: { temple: templeLocation[2], explorer: explorerLocation[2] },
      brown: { temple: templeLocation[3], explorer: explorerLocation[3] }
    }
    setup = this.check(setup, explorerLocation, templeLocation)
    console.log(setup)
    var text = ''// _.join(setup, ' ')
    _.forEach(setup, function (value, key) {
      text = text + ' ' + key + ': temple ' + value.temple + 'º, explorer ' + value.explorer + 'º<br>'
    })
    $('#cardText').html(text)
  },
  check: function (setup, explorerLocation, templeLocation) {
    var newSetup = setup
    _.forEach(setup, function (value, key) {
      var templeLoc = value.temple
      var explorerLoc = value.explorer
      if (templeLoc === explorerLoc === 10) {
        newSetup[key].temple = templeLocation[4]
      } else if (templeLoc === explorerLoc === 110) {
        newSetup[key].temple = templeLocation[5]
      } else if (templeLoc === 110 && explorerLoc === 100) {
        newSetup[key].temple = templeLocation[6]
      } else if (templeLoc === 100 && explorerLoc === 110) {
        newSetup[key].temple = templeLocation[7]
      } else if (templeLoc === 10 && explorerLoc === 20) {
        newSetup[key].temple = templeLocation[8]
      } else if (templeLoc === 20 && explorerLoc === 10) {
        newSetup[key].temple = templeLocation[9]
      }
    })
    return newSetup
  }
}

module.exports = Setup
