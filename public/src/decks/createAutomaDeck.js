var _ = require('lodash')

var AutomaDeck = {
  mode: 'Normal',
  originalautomaDeck: [],
  currentGameAutomaDeck: [],
  getNewDeck: function (mode) {
    var currentGameAutomaDeck = []
    currentGameAutomaDeck = this.getBasicDeck()

    var numberOfLostIntheJungleCards = 0 // expert mode, no lost in the jungle cards
    if (mode === 'Easy') {
      numberOfLostIntheJungleCards = 6
    } else if (mode === 'Normal') {
      numberOfLostIntheJungleCards = 3
    }
    for (var i = 0; i < numberOfLostIntheJungleCards; i++) {
      currentGameAutomaDeck.push(['Lost in the jungle'])
    }
    var suffledAutomaDeck = _.shuffle(currentGameAutomaDeck)
    return _.take(suffledAutomaDeck, 36)
  },
  getBasicDeck () {
    var deck = [
      [1, 'crystal'],
      [1, 'crystal'],
      [1, 'crystal'],
      [2, 'crystal'],
      [2, 'crystal'],
      [2, 'crystal'],
      [3, 'crystal'],
      [3, 'crystal'],
      [3, 'crystal'],

      [1, 'gold'],
      [2, 'gold'],
      [3, 'gold'],

      [1, 'temple', 'blue'],
      [1, 'temple', 'yellow'],
      [1, 'temple', 'brown'],
      [1, 'temple', 'purple'],
      [1, 'explorer', 'blue'],
      [1, 'explorer', 'yellow'],
      [1, 'explorer', 'brown'],
      [1, 'explorer', 'purple'],

      [2, 'temple', 'blue'],
      [2, 'temple', 'yellow'],
      [2, 'temple', 'brown'],
      [2, 'temple', 'purple'],
      [2, 'explorer', 'blue'],
      [2, 'explorer', 'yellow'],
      [2, 'explorer', 'brown'],
      [2, 'explorer', 'purple'],

      [3, 'temple', 'blue'],
      [3, 'temple', 'yellow'],
      [3, 'temple', 'brown'],
      [3, 'temple', 'purple'],
      [3, 'explorer', 'blue'],
      [3, 'explorer', 'yellow'],
      [3, 'explorer', 'brown'],
      [3, 'explorer', 'purple']

    ]
    return deck
  }
}

module.exports = AutomaDeck
