var $ = require('jquery')
// Bootstrap wants jQuery global =(
window.jQuery = $
var _ = require('lodash')
require('popper.js/dist/umd/popper')
require('bootstrap/dist/js/bootstrap')
var AutomaDeck = require('./decks/createAutomaDeck')
var Setup = require('./decks/gamesetup')

var cssify = require('cssify')
// cssify.byUrl('https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css')
cssify.byUrl('./bootstrap.min.css')

// initial setup (randomly place explorers and temples)
$('#setup').click(function (e) {
  Setup.newSetup()
})

// suffle automa deck:
var mode = 'Normal' // default mode
var automaDeck = AutomaDeck.getNewDeck(mode)
var automaDeckSize = _.size(automaDeck)
var automaDeckIndex = 0

// check automa difficulty
$('.automamode').click(function (e) {
  $(this).addClass('active').siblings().removeClass('active')
  mode = e.target.innerText
  automaDeck = AutomaDeck.getNewDeck(mode)
})

// player click when getting a treasure
$('#blue-treasure').click(function (e) {
  e.preventDefault()
  var action = ['none', 'none', 'blue']
  treasureDeck(action, false) // false -> is not the automa who is getting the treasure
})
$('#yellow-treasure').click(function (e) {
  e.preventDefault()
  var action = ['none', 'none', 'yellow']
  treasureDeck(action, false)
})
$('#brown-treasure').click(function (e) {
  e.preventDefault()
  var action = ['none', 'none', 'brown']
  treasureDeck(action, false)
})
$('#purple-treasure').click(function (e) {
  e.preventDefault()
  var action = ['none', 'none', 'purple']
  treasureDeck(action, false)
})

// On the automas turn get the top card of the automa deck:
$('#automaTurn').click(function (e) {
  $('#cardText').html(_.join(automaDeck[automaDeckIndex], ' '))
  e.preventDefault()
  if (automaDeckIndex < automaDeckSize && automaDeck[automaDeckIndex][0] !== 'Lost in the jungle') {
    automaAction(automaDeck[automaDeckIndex])
  }
  automaDeckIndex = automaDeckIndex + 1
})

// blue [temple, explorer], false: the automa did not get them, true: yes
var automaScoring = [
  { blue: [false, false], yellow: [false, false], purple: [false, false], brown: [false, false], crystals: 0, gold: 0, treasures: 0 },
  { blue: [false, false], yellow: [false, false], purple: [false, false], brown: [false, false], crystals: 0, gold: 0, treasures: 0 },
  { blue: [false, false], yellow: [false, false], purple: [false, false], brown: [false, false], crystals: 0, gold: 0, treasures: 0 }
]

function automaAction (action) {
  var automaIndex = action[0] - 1
  var htmlID = ''
  if (action[1] === 'crystal') {
    automaScoring[automaIndex].crystals = automaScoring[automaIndex].crystals + 1
    htmlID = '#' + action[0] + '-crystals'
    $(htmlID).html(automaScoring[automaIndex].crystals + ' crystals')
  } else if (action[1] === 'gold') {
    automaScoring[automaIndex].gold = automaScoring[automaIndex].gold + 1
    htmlID = '#' + action[0] + '-gold'
    $(htmlID).html('1 gold')
  } else {
    automaExplorerAction(action)
    htmlID = '#' + action[0] + '-' + action[2] + '-' + action[1]
    if (action[1] === 'temple') {
      $(htmlID).html(action[2])
    } else {
      $(htmlID).html('X')
    }
  }
  function automaExplorerAction (action) {
    var automaIndex = action[0] - 1
    var cAutomaScoring = automaScoring[automaIndex]

    // add action to automascoring collection:
    if (action[1] === 'temple') {
      cAutomaScoring[action[2]][0] = true // temple
    } else {
      cAutomaScoring[action[2]][1] = true // explorer
    }
    // check for treasures in this color (action[2]):
    if (cAutomaScoring[action[2]][0] && cAutomaScoring[action[2]][1]) {
      treasureDeck(action, true)
    }
    // check if automa has found all temples
    var completedQuest = []
    _.forEach(cAutomaScoring, function (value, key) {
      if (value[0] && value[1]) {
        completedQuest.push(key)
      }
      if (completedQuest.length === 4) {
        alert('endgame')
      }
    })
  }
  // automa total scoring
  var totalPoints = automaScoring[automaIndex].gold +
    automaScoring[automaIndex].crystals +
    automaScoring[automaIndex].treasures
  var automaID = '#automa-' + action[0]
  $(automaID).html(totalPoints)
}
// player and automas common treasures deck
function treasureDeck (action, automa) {
  var treasuresID = '#' + action[2] + '-treasure span'
  var points = _.toNumber($(treasuresID).html())
  var pointsLeft = points - 1
  pointsLeft = ((pointsLeft < 2) ? 0 : pointsLeft)
  $(treasuresID).html(pointsLeft)
  if (automa) {
    var automaIndex = action[0] - 1
    var treasurePoints = automaScoring[automaIndex].treasures + points
    automaScoring[automaIndex].treasures = treasurePoints
    var htmlID = '#' + action[0] + '-treasures'
    $(htmlID).html(automaScoring[automaIndex].treasures)
  }
}
